# day3-Gillian

## Objective: 
I learned code review today. Code review helps us improve code quality, knowledge sharing, and team collaboration.

Then I learned the Java Stream API. With this declarative programming approach, code can be more concise.

I also focused on object-oriented programming. Understand the three characteristics of oo: encapsulation, inheritance, polymorphism.

## Reflective: 
Fulfilled but a litt difficult

## Interpretive: 
The java Streaming API is a great tool, but I haven't gotten into the habit of using it. Today was the first time I used OOP. I was not familiar with some keywords related to Class, so the speed of writing code was very slow. But my study today is a little better than yesterday and I need to keep working hard.

## Decisional: 
Record the java difficulties encountered in class at any time, and study after class. Compare teacher's sample code with my own code after class, and make comments on the code I don't understand.
