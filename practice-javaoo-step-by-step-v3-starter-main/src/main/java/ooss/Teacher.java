package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> teachKlasses = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduce = new StringBuilder(super.introduce());
        introduce.append(" I am a teacher. I teach Class ");
        for (Klass teachKlass: teachKlasses){
            introduce.append(teachKlass.getNumber());
            if (teachKlass.equals(teachKlasses.get(teachKlasses.size() - 1)))
                introduce.append(".");
            else
                introduce.append(", ");
        }
        return introduce.toString();
    }

    public void assignTo(Klass klass){
        this.teachKlasses.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return this.teachKlasses.contains(klass);
    }

    public boolean isTeaching(Student student){
        return this.teachKlasses.contains(student.getKlass());
    }
}
