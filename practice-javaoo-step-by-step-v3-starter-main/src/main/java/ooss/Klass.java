package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private int number;
    private Student leader;
    private List<Teacher> teachers = new ArrayList<>();
    private List<Student> students = new ArrayList<>();

    public Klass(int number){
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public boolean isLeader(Student student){
        return student.equals(this.leader);
    }

    public void assignLeader(Student student){
        if (student.isIn(this)) {
            this.leader = student;
            for (Teacher teacher: teachers) {
                System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n",
                        teacher.name, this.number, student.name);
            }
            for (Student eachStudent: students) {
                System.out.printf("I am %s, student of Class %d. I know %s become Leader.",
                        eachStudent.name, this.number, student.name);
            }
        }
        else
            System.out.println("It is not one of us.");
    }

    public void attach(Teacher teacher) {
        teachers.add(teacher);
    }

    public void attach(Student student){
        students.add(student);
    }
}
