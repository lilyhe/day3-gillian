package ooss;

public class Student extends Person {
    private Klass klass;
    public Student(int id, String name, int age){
        super(id, name, age);
    }

    public Student(int id, String name, int age, Klass klass){
        super(id, name, age);
        this.klass = klass;
    }

    @Override
    public String introduce() {
        if (this.klass.isLeader(this)) {
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",
                    name, age, this.klass.getNumber());
        } else {
            return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",
                    name, age, klass.getNumber());
        }
    }
    public void join(Klass klass){
        this.klass = klass;
    }
    public boolean isIn(Klass klass){
        return klass.equals(this.klass);
    }
    public Klass getKlass(){
        return klass;
    }
}
